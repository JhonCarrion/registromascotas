package com.cooperativa.alex.registromascota.API;

import com.cooperativa.alex.registromascota.response.Mascota;
import com.cooperativa.alex.registromascota.response.Response_API;
import com.cooperativa.alex.registromascota.response.Response_API_Lista;
import com.cooperativa.alex.registromascota.response.Response_API_Object;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class API_Consultas {

    public interface consulta {

        @GET("/obtener_mascota.php")
        Call<Response_API_Lista> getLista();
        @GET("/obtener_mascota_por_id.php")
        Call<Response_API_Object> getById(@Query("id") int mascota_Id);
        @GET("/obtener_propietario_por_mascota.php")
        Call<Response_API_Lista> getOwnerById(@Query("id") int mascota_Id);
        @POST("/insertar_mascota.php")
        //@FormUrlEncoded
        Call<Response_API> postNuevo(@Body Mascota mascota);
        //Call<Response_API> postNuevo(@Field("nombre") String nombre,
        //                             @Field("tipo") String tipo,
        //                             @Field("raza") String raza,
        //                             @Field("genero") String genero,
        //                             @Field("propietario") String propietario,
        //                             @Field("telefono") String telefono,
        //                             @Field("direccion") String direccion);
        @POST("/actualizar_mascota.php")
        Call<Response_API> postEditar(@Body Mascota mascota);
        @POST("/eliminar_mascota.php")
        //@FormUrlEncoded
        Call<Response_API> postEliminar(@Body Mascota mascota);
    }
}
