package com.cooperativa.alex.registromascota;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cooperativa.alex.registromascota.adapter.MascotaAdapter;
import com.cooperativa.alex.registromascota.presenter.Presenter_Detail;
import com.cooperativa.alex.registromascota.presenter.Presenter_Lista;
import com.cooperativa.alex.registromascota.response.Mascota;
import com.cooperativa.alex.registromascota.response.Response_API;
import com.cooperativa.alex.registromascota.response.Response_API_Lista;
import com.cooperativa.alex.registromascota.response.Response_API_Object;
import com.cooperativa.alex.registromascota.services.OnComunicacionLista;
import com.cooperativa.alex.registromascota.services.OnComunicationDetail;
import com.cooperativa.alex.registromascota.utils.GeneralCompat;

import java.util.ArrayList;
import java.util.List;

public class AdminActivity extends GeneralCompat implements OnComunicacionLista {

    private Button btn_buscar;
    private EditText txt_buscar;
    private Presenter_Lista presenter_lista;
    private ProgressDialog progressDialog;
    private RecyclerView mRecyclerView;
    private FloatingActionButton fab;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    protected final static int REQUEST_CODE_MASCOTA_ACTIVITY = 101;
    protected final static int REQUEST_CODE_EDIT_MASCOTA_ACTIVITY = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn_buscar = findViewById(R.id.btn_buscar_x_id);
        txt_buscar = findViewById(R.id.txt_buscar_x_id);

        presenter_lista = new Presenter_Lista(this);
        presenter_lista.Listar();


        fab = (FloatingActionButton) findViewById(R.id.fab_new_mascota);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                // .setAction("Action", null).show();
                startActivityForResult(new Intent(AdminActivity.this, NewActivity.class), REQUEST_CODE_MASCOTA_ACTIVITY);
                finish();
            }
        });
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando Datos");
        progressDialog.show();
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MascotaAdapter(new ArrayList<Mascota>());
        mRecyclerView.setAdapter(mAdapter);
        btn_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = txt_buscar.getText().toString();
                if (id != "") {
                    presenter_lista.obtenerId(Integer.parseInt(id));
                } else {
                    presenter_lista.Listar();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_MASCOTA_ACTIVITY && resultCode == RESULT_OK) {
            Toast.makeText(AdminActivity.this, data.getLongExtra("result", -1) == -1 ? "Error de Guardado" : "Guardado Exitoso", Toast.LENGTH_SHORT).show();
            recreate();
        }
        if (requestCode == REQUEST_CODE_EDIT_MASCOTA_ACTIVITY && resultCode == RESULT_OK) {
            Toast.makeText(AdminActivity.this, data.getIntExtra("result", -1) == -1 ? "Error de Modificado" : "Modificado Exitoso", Toast.LENGTH_SHORT).show();
            recreate();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void cargarDatos(final List<Mascota> mascotas) {
        mAdapter = new MascotaAdapter(mascotas);
        Log.e("respuesta", String.valueOf(mAdapter.getItemCount()));
        ((MascotaAdapter) mAdapter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final CharSequence[] options = {"Editar", "Eliminar", "Cancelar"};
                AlertDialog.Builder builder = new AlertDialog.Builder(AdminActivity.this);
                builder.setTitle("Opciones")
                        .setItems(options, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                final Mascota mascota = mascotas.get(mRecyclerView.getChildAdapterPosition(v));
                                if (mascota.getNombre() != null) {
                                    if (options[which].equals("Editar")) {
                                        Intent intent = new Intent(AdminActivity.this, NewActivity.class);
                                        intent.putExtra("mascota_id", mascota.getMascota_id());
                                        intent.putExtra("nombre", mascota.getNombre());
                                        intent.putExtra("direccion", mascota.getDireccion());
                                        intent.putExtra("telefono", mascota.getTelefono());
                                        intent.putExtra("raza", mascota.getRaza());
                                        intent.putExtra("tipo", mascota.getTipo());
                                        intent.putExtra("propietario", mascota.getPropietario());
                                        intent.putExtra("genero", mascota.getGenero());
                                        startActivityForResult(intent, REQUEST_CODE_EDIT_MASCOTA_ACTIVITY);
                                        finish();
                                    } else if (options[which].equals("Eliminar")) {
                                        createSimpleDialog(AdminActivity.this, "ATENCIÓN", "Esta seguro de Eliminar a:\n" + mascota.getNombre(), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                progressDialog = new ProgressDialog(AdminActivity.this);
                                                progressDialog.setMessage("Cargando Datos");
                                                progressDialog.show();
                                                Presenter_Detail presenter_detail = new Presenter_Detail(new OnComunicationDetail() {
                                                    @Override
                                                    public void respuestaEnviarMascota(Response_API response) {
                                                        progressDialog.dismiss();
                                                        Toast.makeText(AdminActivity.this, response.getMensaje(), Toast.LENGTH_SHORT).show();
                                                        recreate();
                                                        cerrarDialog();
                                                    }

                                                    @Override
                                                    public void errorEnviarMascota(Response_API response_api) {
                                                        progressDialog.dismiss();
                                                        Toast.makeText(AdminActivity.this, response_api.getMensaje(), Toast.LENGTH_SHORT).show();
                                                        finish();
                                                    }
                                                });
                                                presenter_detail.eliminar(mascota);
                                            }
                                        }, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                cerrarDialog();
                                            }
                                        }).show();
                                    } else {

                                    }
                                }
                            }
                        });
                builder.create().show();
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void respuestaListaMascota(final Response_API_Lista response) {
        // specify an adapter (see also next example)
        progressDialog.dismiss();
        if (response.getEstado() == 1) {
            cargarDatos(response.getMascota());
        } else {
            Toast.makeText(AdminActivity.this, "No hay Registros de Mascotas", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void errorListaMascota(Response_API_Lista response_api) {
        progressDialog.dismiss();
        Toast.makeText(AdminActivity.this, response_api.getMensaje(), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void respuestaObjectMascota(Response_API_Object response) {
        progressDialog.dismiss();
        if (response.getEstado() == 1) {
            List<Mascota> mascotas = new ArrayList<Mascota>();
            mascotas.add(response.getMascota());
            cargarDatos(mascotas);
        } else {
            Toast.makeText(AdminActivity.this, "No hay Registros de Mascotas", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void errorObjectMascota(Response_API_Object response_api) {
        progressDialog.dismiss();
        Toast.makeText(AdminActivity.this, response_api.getMensaje(), Toast.LENGTH_SHORT).show();
        finish();
    }
}
