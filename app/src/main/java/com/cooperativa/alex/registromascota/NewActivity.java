package com.cooperativa.alex.registromascota;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.cooperativa.alex.registromascota.presenter.Presenter_Detail;
import com.cooperativa.alex.registromascota.response.Mascota;
import com.cooperativa.alex.registromascota.response.Response_API;
import com.cooperativa.alex.registromascota.services.OnComunicationDetail;

public class NewActivity extends AppCompatActivity implements OnComunicationDetail {

    private Presenter_Detail presenter_detail;
    private EditText txtNombre, txtTipo, txtRaza, txtPropietario, txtTelefono, txtDireccion;
    private String txtGenero;
    private int mascota_id;
    private Button btnCancelar, btnRegistrar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.tipo_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
        mascota_id = getIntent().getIntExtra("mascota_id", 0);
        init();
        if (mascota_id != 0) {
            final String nombre = getIntent().getStringExtra("nombre");
            final String direccion = getIntent().getStringExtra("direccion");
            final String propietario = getIntent().getStringExtra("propietario");
            final String raza = getIntent().getStringExtra("raza");
            final String telefono = getIntent().getStringExtra("telefono");
            final String tipo = getIntent().getStringExtra("tipo");
            final String genero = getIntent().getStringExtra("genero");
            txtDireccion.setText(direccion);
            txtNombre.setText(nombre);
            txtPropietario.setText(propietario);
            txtRaza.setText(raza);
            txtTelefono.setText(telefono);
            txtTipo.setText(tipo);
            if (genero.equals("Macho")) {
                spinner.setSelection(0);
            } else {
                spinner.setSelection(1);
            }
        }
    }

    private void init() {
        txtDireccion = findViewById(R.id.txt_mascota_direccion);
        txtNombre = findViewById(R.id.txt_mascota_nombre);
        txtPropietario = findViewById(R.id.txt_mascota_propietario);
        txtRaza = findViewById(R.id.txt_mascota_raza);
        txtTelefono = findViewById(R.id.txt_mascota_telefono);
        txtTipo = findViewById(R.id.txt_mascota_tipo);

        btnCancelar = findViewById(R.id.btn_mascota_cancelar);
        btnRegistrar = findViewById(R.id.btn_mascota_guardar);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner sp = (Spinner) findViewById(R.id.spinner);
                txtGenero = sp.getSelectedItem().toString();
                int nPos = sp.getSelectedItemPosition();
                progressDialog = new ProgressDialog(NewActivity.this);
                progressDialog.setMessage("Enviando Datos");
                progressDialog.show();
                presenter_detail = new Presenter_Detail(NewActivity.this);
                Mascota mascota = new Mascota(txtNombre.getText().toString(),
                        txtTipo.getText().toString(),
                        txtRaza.getText().toString(),
                        txtGenero,
                        txtPropietario.getText().toString(),
                        txtTelefono.getText().toString(),
                        txtDireccion.getText().toString());
                if (mascota_id != 0) {
                    mascota.setMascota_id(mascota_id);
                    presenter_detail.editar(mascota);
                } else {
                    presenter_detail.guardar(mascota);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void respuestaEnviarMascota(Response_API response) {
        progressDialog.dismiss();
        Toast.makeText(NewActivity.this, response.getMensaje(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(NewActivity.this, AdminActivity.class));
        finish();
    }

    @Override
    public void errorEnviarMascota(Response_API response_api) {
        progressDialog.dismiss();
        Toast.makeText(NewActivity.this, response_api.getMensaje(), Toast.LENGTH_SHORT).show();
        finish();
    }
}
