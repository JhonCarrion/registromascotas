package com.cooperativa.alex.registromascota.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cooperativa.alex.registromascota.R;
import com.cooperativa.alex.registromascota.response.Mascota;

import java.util.List;

public class MascotaAdapter extends RecyclerView.Adapter<MascotaAdapter.MyViewHolder> implements View.OnClickListener {

    private List<Mascota> mDataset;

    private View.OnClickListener onClickListener;


    @Override
    public void onClick(View v) {
        if (onClickListener != null) {
            onClickListener.onClick(v);
        }
    }

    public void setOnClickListener(View.OnClickListener listener) {
        onClickListener = listener;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView lblNombre, lblTipo, lblRaza, lblGenero, lblPropietario, lblTelefono, lblDireccion;

        public MyViewHolder(View v) {
            super(v);
            lblNombre = v.findViewById(R.id.lbl_item_mascota_nombre);
            lblTipo = v.findViewById(R.id.lbl_item_mascota_tipo);
            lblRaza = v.findViewById(R.id.lbl_item_mascota_raza);
            lblGenero = v.findViewById(R.id.lbl_item_mascota_genero);
            lblPropietario = v.findViewById(R.id.lbl_item_mascota_owner);
            lblTelefono = v.findViewById(R.id.lbl_item_mascota_telefono);
            lblDireccion = v.findViewById(R.id.lbl_item_mascota_direccion);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MascotaAdapter(List<Mascota> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MascotaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mascota, parent, false);
        v.setOnClickListener(this);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.lblNombre.setText(mDataset.get(position).getNombre());
        holder.lblTipo.setText(mDataset.get(position).getTipo());
        holder.lblRaza.setText(mDataset.get(position).getRaza());
        holder.lblGenero.setText(mDataset.get(position).getGenero());
        holder.lblPropietario.setText(mDataset.get(position).getPropietario());
        holder.lblTelefono.setText(mDataset.get(position).getTelefono());
        holder.lblDireccion.setText(mDataset.get(position).getDireccion());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
