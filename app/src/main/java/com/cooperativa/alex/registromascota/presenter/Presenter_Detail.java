package com.cooperativa.alex.registromascota.presenter;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.cooperativa.alex.registromascota.API.API_Consultas;
import com.cooperativa.alex.registromascota.response.Mascota;
import com.cooperativa.alex.registromascota.response.Response_API;
import com.cooperativa.alex.registromascota.services.OnComunicationDetail;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Presenter_Detail extends Presenter {

    private OnComunicationDetail onComunicationDetailMascota;

    public Presenter_Detail(OnComunicationDetail onComunicacionDetailMascota) {
        this.onComunicationDetailMascota = onComunicacionDetailMascota;
    }

    public void guardar(Mascota mascota) {

        API_Consultas.consulta service = retrofit.create(API_Consultas.consulta.class);

        Call<Response_API> call = service.postNuevo(mascota);

        call.enqueue(new Callback<Response_API>() {

            @Override
            public void onResponse(@NonNull Call<Response_API> call, @NonNull Response<Response_API> response) {
                Response_API responseApi = response.body();
                if (responseApi != null) {
                    if (response.code() == 200) {
                        Log.e("respuesta200", "200");
                        onComunicationDetailMascota.respuestaEnviarMascota(responseApi);
                    } else {
                        Log.e("respuesta200", "no200");
                        onComunicationDetailMascota.errorEnviarMascota(responseApi);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response_API> call, @NonNull Throwable t) {
                Log.e("errorWS", t.getMessage());
                onComunicationDetailMascota.errorEnviarMascota(new Response_API(0, "Problema con el Servidor"));
            }

        });
    }

    public void editar(Mascota mascota) {

        API_Consultas.consulta service = retrofit.create(API_Consultas.consulta.class);

        Call<Response_API> call = service.postEditar(mascota);

        call.enqueue(new Callback<Response_API>() {

            @Override
            public void onResponse(@NonNull Call<Response_API> call, @NonNull Response<Response_API> response) {
                Response_API responseApi = response.body();
                if (responseApi != null) {
                    if (response.code() == 200) {
                        Log.e("respuesta200", "200");
                        onComunicationDetailMascota.respuestaEnviarMascota(responseApi);
                    } else {
                        Log.e("respuesta200", "no200");
                        onComunicationDetailMascota.errorEnviarMascota(responseApi);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response_API> call, @NonNull Throwable t) {
                Log.e("errorWS", t.getMessage());
                onComunicationDetailMascota.errorEnviarMascota(new Response_API(0, "Problema con el Servidor"));
            }

        });
    }

    public void eliminar(Mascota mascota) {

        API_Consultas.consulta service = retrofit.create(API_Consultas.consulta.class);

        Call<Response_API> call = service.postEliminar(mascota);

        call.enqueue(new Callback<Response_API>() {

            @Override
            public void onResponse(@NonNull Call<Response_API> call, @NonNull Response<Response_API> response) {
                Response_API responseApi = response.body();
                if (responseApi != null) {
                    if (response.code() == 200) {
                        Log.e("respuesta200", "200");
                        onComunicationDetailMascota.respuestaEnviarMascota(responseApi);
                    } else {
                        Log.e("respuesta200", "no200");
                        onComunicationDetailMascota.errorEnviarMascota(responseApi);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response_API> call, @NonNull Throwable t) {
                Log.e("errorWS", t.getMessage());
                onComunicationDetailMascota.errorEnviarMascota(new Response_API(0, "Problema con el Servidor"));
            }

        });
    }
}