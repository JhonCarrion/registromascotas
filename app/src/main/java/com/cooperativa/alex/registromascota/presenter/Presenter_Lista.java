package com.cooperativa.alex.registromascota.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.cooperativa.alex.registromascota.API.API_Consultas;
import com.cooperativa.alex.registromascota.response.Response_API_Lista;
import com.cooperativa.alex.registromascota.response.Response_API_Object;
import com.cooperativa.alex.registromascota.services.OnComunicacionLista;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Presenter_Lista extends Presenter {

    private OnComunicacionLista onComunicacionListarMascota;

    public Presenter_Lista(OnComunicacionLista onComunicacionListar) {
        this.onComunicacionListarMascota = onComunicacionListar;
    }

    public void Listar() {

        API_Consultas.consulta service = retrofit.create(API_Consultas.consulta.class);

        Call<Response_API_Lista> call = service.getLista();

        call.enqueue(new Callback<Response_API_Lista>() {

            @Override
            public void onResponse(@NonNull Call<Response_API_Lista> call, @NonNull Response<Response_API_Lista> response) {
                Response_API_Lista responseApi = response.body();
                if (responseApi != null) {
                    if (response.code() == 200) {
                        Log.e("respuesta200", "200");
                        onComunicacionListarMascota.respuestaListaMascota(responseApi);
                    } else {
                        Log.e("respuesta200", "no200");
                        onComunicacionListarMascota.errorListaMascota(responseApi);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response_API_Lista> call, @NonNull Throwable t) {
                Log.e("errorWS", t.getMessage());
                Response_API_Lista res= new Response_API_Lista();
                res.setEstado(0);
                res.setMensaje("El servidor no Responde");
                onComunicacionListarMascota.errorListaMascota(res);
            }

        });
    }
    public void obtenerId(int id) {

        API_Consultas.consulta service = retrofit.create(API_Consultas.consulta.class);

        Call<Response_API_Object> call = service.getById(id);

        call.enqueue(new Callback<Response_API_Object>() {

            @Override
            public void onResponse(@NonNull Call<Response_API_Object> call, @NonNull Response<Response_API_Object> response) {
                Response_API_Object responseApi = response.body();
                if (responseApi != null) {
                    if (response.code() == 200) {
                        Log.e("respuesta200", "200");
                        onComunicacionListarMascota.respuestaObjectMascota(responseApi);
                    } else {
                        Log.e("respuesta200", "no200");
                        onComunicacionListarMascota.errorObjectMascota(responseApi);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response_API_Object> call, @NonNull Throwable t) {
                Log.e("errorWS", t.getMessage());
                Response_API_Object res= new Response_API_Object();
                res.setEstado(0);
                res.setMensaje("El servidor no Responde");
                onComunicacionListarMascota.errorObjectMascota(res);
            }

        });
    }

}
