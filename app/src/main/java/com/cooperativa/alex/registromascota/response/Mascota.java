package com.cooperativa.alex.registromascota.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mascota {

    @SerializedName("mascota_id")
    @Expose
    private int mascota_id;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("raza")
    @Expose
    private String raza;
    @SerializedName("genero")
    @Expose
    private String genero;
    @SerializedName("propietario")
    @Expose
    private String propietario;
    @SerializedName("telefono")
    @Expose
    private String telefono;
    @SerializedName("direccion")
    @Expose
    private String direccion;

    public Mascota(String nombre, String tipo, String raza, String genero, String propietario, String telefono, String direccion) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.raza = raza;
        this.genero = genero;
        this.propietario = propietario;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    public Mascota() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getMascota_id() {
        return mascota_id;
    }

    public void setMascota_id(int mascota_id) {
        this.mascota_id = mascota_id;
    }

    @Override
    public String toString() {
        return "Mascota{" +
                "nombre='" + nombre + '\'' +
                ", tipo='" + tipo + '\'' +
                ", raza='" + raza + '\'' +
                ", genero='" + genero + '\'' +
                ", propietario='" + propietario + '\'' +
                ", telefono='" + telefono + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
