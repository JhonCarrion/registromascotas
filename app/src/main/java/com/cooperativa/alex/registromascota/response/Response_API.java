package com.cooperativa.alex.registromascota.response;

public class Response_API {
    private int estado;
    private String mensaje;

    public Response_API() {
    }

    public Response_API(int estado, String mensaje) {
        this.estado = estado;
        this.mensaje = mensaje;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "Response_API{" +
                "estado=" + estado +
                ", mensaje=" + mensaje +
                '}';
    }
}
