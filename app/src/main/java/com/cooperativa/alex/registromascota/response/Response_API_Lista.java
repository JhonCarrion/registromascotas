package com.cooperativa.alex.registromascota.response;

import java.util.List;

public class Response_API_Lista extends Response_API{
    private List<Mascota> mascota;

    public List<Mascota> getMascota() {
        return mascota;
    }

    public void setMascota(List<Mascota> mascota1) {
        this.mascota = mascota1;
    }

    @Override
    public String toString() {
        return "Response_API_Lista{" +
                "mascota=" + mascota +
                '}';
    }
}
