package com.cooperativa.alex.registromascota.response;

public class Response_API_Object extends Response_API {
    private Mascota mascota;

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota1) {
        this.mascota = mascota1;
    }

    @Override
    public String toString() {
        return "Response_API_Lista{" +
                "mascota=" + mascota +
                '}';
    }
}
