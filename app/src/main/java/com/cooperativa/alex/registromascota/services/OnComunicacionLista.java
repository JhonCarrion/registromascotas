package com.cooperativa.alex.registromascota.services;

import com.cooperativa.alex.registromascota.response.Response_API_Lista;
import com.cooperativa.alex.registromascota.response.Response_API_Object;

public interface OnComunicacionLista {
    void respuestaListaMascota(Response_API_Lista response);

    void errorListaMascota(Response_API_Lista response_api);

    void respuestaObjectMascota(Response_API_Object response);

    void errorObjectMascota(Response_API_Object response_api);
}
