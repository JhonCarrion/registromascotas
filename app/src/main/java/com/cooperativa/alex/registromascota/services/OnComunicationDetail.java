package com.cooperativa.alex.registromascota.services;

import com.cooperativa.alex.registromascota.response.Response_API;

public interface OnComunicationDetail {
    void respuestaEnviarMascota(Response_API response);

    void errorEnviarMascota(Response_API response_api);
}
